from time import time


def performance(func):
    def wrapper(*args, **kwargs):
        t1 = time()
        func(*args, **kwargs)
        t2 = time()
        print(f"Time complexity: {(t2 - t1)}ms")
    return wrapper

@performance
def main():
    try:
        n = int(input("Enter number of terms needed: "))
        f = 0
        s = 1
        print(f)
        print(s)
        for i in range(2,n):
            temp = f+s
            print(temp)
            f = s
            s = temp
    except Exception as e:
        print(e)

if __name__ == "__main__":
    main()
import flask

app = flask.Flask(__name__)



@app.route("/portfolio")
def home():
    return flask.render_template("index.html")

@app.route("/portfolio/about")
def about():
    return flask.render_template("about.html")

@app.route("/portfolio/timeline")
def timeline():
    return flask.render_template("timeline.html")

@app.route("/portfolio/projects")
def projects():
    return flask.render_template("projects.html")

@app.route("/portfolio/achievements")
def achieve():
    return flask.render_template("achievements.html")

@app.route("/favicon.ico") # provides favicom to each and every endpoint in flask
def favicon():
    return flask.send_file("./static/img/am.ico")

@app.route("/favi")
def favi():
    return "dasfwf"

@app.route("/portfolio/contact-us", methods=['GET', 'POST'])
def contact():
    if flask.request.method == 'POST':
        print(fname:=flask.request.form['fname'],
        lname:=flask.request.form['lname'],
        email:=flask.request.form['email'],
        phone:=flask.request.form['phone'],
        subject:=flask.request.form['subject'],
        gender:=flask.request.form['gender'],
        message:=flask.request.form['message'])
        # OR
        print(data := flask.request.form.to_dict())

        # Store data in file database
        with open("database",mode='a') as db:
            db.write(f"\n{data}")
        
        # Store data in csv-based database
        with open("db.csv", mode='a') as db:
            db.write(f"\n{fname},{lname},{email},{phone},{subject},{gender},{message}")
        return flask.render_template("contact.html",st=f"Thank you {data['fname']} for contacting!!")
    return flask.render_template("contact.html")

@app.route("/test")
def test1():
    return flask.render_template("test.html")

@app.route("/test/<string:name>")
def test2(name):
    print(name)
    return flask.render_template("test.html", name=name)

@app.route("/<string:template>")
def renderT(template):
    """This route uses html template name from path variable and renders it"""
    return flask.render_template(template)


if __name__ == "__main__":
    app.run(debug=True)

import math, time

print(round(3.4))

print(abs(-43))

print(math.sqrt(49))

print(bin(5))  #Output: 0b101

a = 12
print(id(a))

print(int("0b101", base=2))  #Output: 5

b = type(a)

print(b)

print(f'your age is around: {time.localtime().tm_year - int(input("Enter your birth year: "))}')

##############################################################################

d = {}

l = ['a23z', '32yd', '8hd', 'ssd', '34345']

print(f"Before: {l}")
for s in l:
    sum = 0
    if s.isalpha():
        # print("alpha")
        sum = 1
    elif s.isdigit():
        # print("num")
        for c in s:
            sum += int(c)
    elif s.isalnum():
        # print("alpha num")
        for c in s:
            if c.isdigit():
                sum += int(c)
        if sum == 0:
            sum = 1

    d[s] = sum

# print(list(d.items()))
ind = 0
for val, _ in sorted(list(d.items()),key= lambda x: x[1]):
    l[ind] = val
    ind += 1

print(f"After: {l}")

string = ",".join(l)
print(string)

##############################################################################

# List Unpacking

a, b, c, *other, d = list(range(1,11))

print(f"a: {a}\nb: {b}\nc: {c}\nother: {other}\nd: {d}")


t = tuple(range(11))

print(len(tuple(sorted(reversed(t)))))

ls = [1,2,3,4,5,6,5,4,2]

print(list(set(ls)))

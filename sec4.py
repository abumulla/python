d = {
    "name" : "Abu",
    "age" : 23,
    "is_alive" : True
}

for _ in enumerate(d.items()):
    print(_)

##############################################################################

for i in range(10,0,-1):
    print(i)

##############################################################################

def display(pic):
    ''' 
    This functions prints the pattern based on 0s and 1s present in 2D array/list
    '''
    space = " "
    star = "*"
    for l in pic:
        for p in l:
            if p == 0:
                print(space,end="")
            elif p == 1:
                print(star,end="")
        print()

picture = [
  [0,0,0,1,0,0,0],
  [0,0,1,1,1,0,0],
  [0,1,1,1,1,1,0],
  [1,1,1,1,1,1,1],
  [0,0,0,1,0,0,0],
  [0,0,0,1,0,0,0]
]

for _ in range(1):
    display(picture)

print("*"*100)

print(display.__doc__)

print("*"*100)

help(display)

print("*"*100)

##############################################################################

l =  list("svdkeyfdaycsdyvdctzvxjbfgowieugdwslqkn")

d = {}
print("Duplicate characters are:")
for e in l:
    d[e] = d.get(e,0) + 1
    if d.get(e,0) == 2:
        print(e)
# print(list(d.items()))
# OR
dup = []
for v in l:
    if l.count(v)>1:
        dup.append(v)
print(list(set(dup)))
print([i for i in set(dup)])

##############################################################################

# Excersise
def checkDriverAge(age=0):
    if int(age) < 18:
        print("Sorry, you are too young to drive this car. Powering off")
    elif int(age) > 18:
        print("Powering On. Enjoy the ride!");
    elif int(age) == 18:
        print("Congratulations on your first year of driving. Enjoy the ride!")

checkDriverAge(int(input("Enter your age: ")))

##############################################################################

class Design:
    def display(self): ...
    def store(self,data): ...
    def get(self, id): ...

class Impl(Design):
    def __init__(self) -> None:
        super().__init__()
        super().display()
        pass

i = Impl()

##############################################################################

def do_it1(*args):
    print(type(args))
    print(args)
def do_it2(**kwargs):
    print(type(kwargs))
    print(kwargs)
def do_it3(*args, **kwargs):
    print(args)
    print(kwargs)

do_it1(1,2,3)
print("*"*100)
do_it2(n1=1,n2=2,n3=3)
print("*"*100)
do_it3(1,2,3,n1=1,n2=2,n3=3)

##############################################################################

def high_even(l:list):
    for e in sorted(l,reverse=True):
        if e%2 == 0:
            print(f"Highest even in list: {e}")
            break

high_even([10,2,3,4,11,8,45,343,87])

##############################################################################

# Demonstration of Walrus Operator
numbers = ["1", "2", "3", "4", "5"]

while (n := len(numbers)) > 0:
	print(n)
	print(type(numbers.pop()))

s = {"fhwkgf"}

s.discard()

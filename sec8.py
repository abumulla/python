def func(func1):
    func1()

def func1():
    print("Function 1!!")

func(func1)
##############################################################################
def func2():
    def func3():
        print("Function 3!!")
    return func3

print(f := func2())
f()

##############################################################################

# Decorators: adds extra functionality to a function
from time import time

def decorator(func):
    def wrapper(*args, **kwargs):
        print("*"*100)
        func(*args, **kwargs)
        print("*"*100)
    return wrapper

@decorator
def hello(name, age=0):
    print(f"Helooooooooo!!{name} and age: {age}")

hello("Abu Mulla")
##############################################################################
def performance(func):
    def wrapper(*args, **kwargs):
        t1 = time()
        func(*args, **kwargs)
        t2 = time()
        print(f"Time complexity: {(t2 - t1)}ms")
    return wrapper

@performance
def do_for():
    for i in range(1000000):
        i**2

do_for()


##############################################################################

user1 = {
    "name": "Sorna",
    "valid": True,  # changing this will either run or not run the message_friends function.
}


def authenticated(fn):
    # code here
    def wrapper(*args, **kwargs):
        if args[0]["valid"]:
            fn(*args, **kwargs)
        else:
            print("invalid user")

    return wrapper


@authenticated
def message_friends(user):
    print("message has been sent")


message_friends(user1)

class Objective:
    def __init__(self, name, weightage) -> None:
        self.name = name
        self.weightage = weightage
        self.is_completed = False
    
    def completed(self):    # Instance method: recieves instance reference in paramet
        self.is_completed = True
        return "Completed!!"

    @classmethod    # class method: not a instance method like above 
    def check(cls, name, age):
        print(name,age)

    @staticmethod   #static method doesn't require self: reference to current object to run itself
    def check2(name,age):
        print(name,age)
    
    def __str__(self) -> str:
        return self.name+" : "+str(self.weightage)

obj = Objective("Training", 100)
print(type(obj))
print(obj.is_completed)
print(obj.completed())
print(obj.is_completed)
Objective.check("abu",22)
Objective.check2("abu",22)
print(isinstance(obj, Objective))

##############################################################################

class Pets():
    def __init__(self, kind, lazy):
        self.kind = kind
        self.is_lazy = lazy

class Cat(Pets):
    def __init__(self, name, age, kind, lazy):
        super().__init__(kind,lazy) #calling super constructor
        self.name = name
        self.age = age

    def walk(self):
        return f'{self.name} is just walking around and lazy = {self.is_lazy}'

my_cat = Cat("Billooo", 2, "animal", True)

print(my_cat.walk())

##############################################################################


class New:
    def __init__(self, name, weightage) -> None:
        self.__name = name  # Private attribute
        self._weightage = weightage # Protected attribute
        self.is_completed = False   
    # Getter and Setter for private attribute
    def setName(self, name):
        self.__name = name
    def getName(self):
        return self.__name
    def __str__(self) -> str:
        return f"{self.__name} : {self._weightage}"
    

new = New("Abubakar", 100)
print(dir(new))

##############################################################################

class A:
    n = 1
class B(A):
    n = 2
class C(A):
    n = 3
class D(B,C):
    n = 4

print(D.mro())

# Method Resolution Order(MRO)

#      object (5)
#        |
#        A (4)
#      /   \
#     /     \
#(2) B       C (3)
#     \     /
#      \   /
#    (1) D


file = open("test.py")

print(file.read())

print(file.readline())
print(file.readline())
print(file.readline())

print(file.readlines())
file.close()

with open("test.py", mode="r") as file:
    print(file.read())

with open("test.py", mode="a") as file:
    file.write("\n# End")

with open("fastapi-dev/main.py", mode="r") as file:
    print(file.read())


try:
    with open("fastapi-dev/main.py", mode="r") as file:
        print(file.read())
except FileNotFoundError as e:
    print("File not found!")
except IOError as e:
    print("IO Error!")
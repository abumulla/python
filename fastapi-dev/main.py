from typing import Union

from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()

class Item(BaseModel):
    name: str
    price: float
    is_offer: Union[bool, None] = None

items = []

@app.get("/home")
def read_root(name : Union[str, None] = None):
    return {"Hello": name}

@app.get("/items/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
    return {"item_id": item_id, "q": q}

@app.post("/items/{item_id}")
def store_item(item_id : int, item : Item):
    items.append(item)
    return {"item_id": item_id, "item": item}

@app.get("/items")
def get_items():
    return {"items": items}
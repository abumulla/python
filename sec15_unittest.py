import unittest

class Test1(unittest.TestCase):
    def setUp(self) -> None:    # works like @Before present in java, that runs before running each test case
        print("Entering a test case")
        return super().setUp()

    def testMax(self):
        """Test Max"""
        self.assertEqual(max(range(1,7)),6)
    def testMin(self):
        self.assertEqual(min(range(1,7)),1)
    def testSum(self):
        self.assertEqual(sum(range(1,7)),21,"djdvfgsdvfjasdfkgsdaf")

    def tearDown(self) -> None:    # works like @After present in java, that runs after running each test case
        print("Exiting from test case")
        return super().tearDown()

if __name__ == "__main__":
    unittest.main()
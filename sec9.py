try:
    c = 1/"324"
except Exception as e:
    print(e)

##############################################################################

try:
    a = 1/0
    b = 1/"23"
except (TypeError,ZeroDivisionError) as e:
    print(e)

##############################################################################

try:
    a = 1/0
except Exception as e:
    print(e)
else:   # Runs only if try block runs successfully without any error or exception
    print("No Error")
finally: # Runs always, irrespective of whether any error or exception is raised or not raised
    print("End!!")

##############################################################################
# Raise Custom Exception
try:
    raise ValueError("Errorrrrrrrr")
except Exception as e:
    print(e)
# Generators: a function that generates a series of values/data without allocating any memory
def generator1(num):
    for j in range(num):
        yield j

print(generator1(10))

for i in generator1(10):
    print(i,end=",")
print()

"""
range() is also a generator that allots a series of values without allocating any memory
"""
print(list(generator1(10)))
# OR
print(list(range(10)))



